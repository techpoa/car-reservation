<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cars extends Model
{
    //
    protected $table = 'dbProj_cars';

    protected $fillable = [
        'make_id', 'model_id', 'reg_number', 'color', 'description', 'year', 'fuel_type', 'drive', 'mileage', 'price', 'active', 'created_at',
    ];
}
