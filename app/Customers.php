<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    //
    protected $table = 'dbProj_customers';

    protected $fillable = [
        'first_name', 'last_name', 'email', 'phone', 'active', 'created_at',
    ];
}
