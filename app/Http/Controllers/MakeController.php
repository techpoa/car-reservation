<?php

namespace App\Http\Controllers;

use App\Cars;
use App\Make;
use App\Traits\FormatAjaxValidationMessages;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class MakeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:dbProj_make',
        ]);
        //
        if($validator -> passes())  //
        {
            $Make = new Make();

            $Make->name = $request->input('name');
            $Make->description = $request->input('description');
            $Make->active = 1;
            $Make->created_at = Carbon::now();

            if ($Make->save())
            {
                return response()->json(['status' => '00', 'message' => 'Created '.$Make -> name .' make successfully']);

            }
            else{
                return response()->json(['status' => '01', 'message' => 'Error when creating make']);
            }
        }
        else{

            return response()->json(['status' => '01', 'message' => $this -> returnMessageString($validator->errors()->getMessages())]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $Make = Make::find($id);

        $response = array(
            "status" => "00",
            "id" => $Make['id'],
            "name" => $Make['name'],
            "description" => $Make['description'],
        );

        return response()->json($response);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:dbProj_make',
        ]);
        //
        if($validator -> passes())
        {
            $Make = Make::findOrFail($id);
            $input = $request->all();

            if ($Make->fill($input)->save())
            {
                return response()->json(['status' => '00', 'message' => 'Updated '. $Make -> name.' make Successfully']);
            }
            else {
                return response()->json(['status' => '01', 'error' => 'Error Updating '. $Make -> name.' make.']);
            }
        }
        else{
            return response()->json(['status' => '01', 'message' => $this -> returnMessageString($validator->errors()->getMessages())]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $Make = Make::find($id);

        $cars_make = Cars::where('make_id', 'like', $Make -> id)
            ->get();

        if ($cars_make->isEmpty())
        {
            $Make = Make::findOrFail($id);
            $Make->delete();

            return response()->json(['status' => '00', 'message' => 'Car make has been Deleted Successfully']);
        }
        else{
            return response()->json(['status' => '01', 'message' => 'Car make has an associated listed car -- Cannot be deleted']);
        }
    }

    public function makeData()
    {

        $makes = Make::all();

        return Datatables::of($makes)
            ->editColumn('active', function ($make) {
                if ($make->active == 1)
                {
                    return '
                    <a href="#" type="button" class="btn btn-success btn-xs deactivate" data-toggle="modal" data-id="'.$make->id .'"
                    data-name="'.$make->name .'" title="Deactivate Make" data-target="#deactivateModal" > Active </a>
                    ';
                }
                else{
                    return '
                    <a href="#" type="button" class="btn btn-warning btn-xs activate" data-toggle="modal" data-id="'.$make->id .'"
                data-name="'.$make->name .'" title="Activate Make" data-target="#activateModal" > Inactive </a>
                    ';
                }
            })
            ->editColumn('actions', function ($make) {
                return '
                 <a href="#" type="button" class="btn btn-primary btn-xs edit" data-toggle="modal" data-id="'.$make->id .'"
                data-name="'.$make->name .'" title="Edit Make" data-target="#editModal" > <i class="fa fa-edit"></i></a>

                <a href="#" type="button" class="btn btn-danger btn-xs delete"  data-toggle="modal" data-id="'.$make->id .'"
                 data-name="'.$make->name .'" title="Delete Make" data-target="#deleteModal"> <i class="fa fa-trash-o"></i> </a>

                ';
            })
            ->rawColumns(['active','actions'])
            ->make(true);

    }


    public function activate(Request $request){

        $id = $request ->input('id');

        Make::where('id', $id)
            ->update(['active' => 1]);

        return response()->json(['status' => '00', 'message' => 'Make has been Activated Successfully']);

    }

    public function deactivate(Request $request){

        $id = $request ->input('id');

        Make::where('id', $id)
            ->update(['active' => 2]);

        return response()->json(['status' => '00', 'message' => 'Make has been deactivated Successfully']);

    }


}
