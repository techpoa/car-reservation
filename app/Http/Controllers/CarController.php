<?php

namespace App\Http\Controllers;

use App\Cars;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Yajra\DataTables\DataTables;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'reg_number' => 'required|unique:dbProj_cars',
        ]);
        //
        if ($validator->passes()) {
            $Car = new Cars();

            $Car->make_id = $request->input('make');
            $Car->model_id = $request->input('model');
            $Car->reg_number = $request->input('reg_number');
            $Car->color = $request->input('color');
            $Car->description = $request->input('description');
            $Car->year = $request->input('year');
            $Car->fuel_type = $request->input('fuel_type');
            $Car->drive = $request->input('drive');
            $Car->mileage = $request->input('mileage');
            $Car->active = 1;
            $Car->created_at = Carbon::now();

            if ($request->file('image')) {
                $image = time() . '.' . $request->image->extension();
                $request->image->move(public_path('uploads/cars'), $image);
                $Car->image_path = "uploads/cars" . $image;

            } else {
                $Car->image_path = '';
            }

            if ($Car->save()) {
                return response()->json(['status' => '00', 'message' => 'Created ' . $Car->reg_number . '  successfully']);

            } else {
                return response()->json(['status' => '01', 'message' => 'Error when entering car details']);
            }
        } else {

            return response()->json(['status' => '01', 'message' => $this->returnMessageString($validator->errors()->getMessages())]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $Car = Cars::find($id);

        $response = array(
            "status" => "00",
            "id" => $Car['id'],
            "make_id" => $Car['make_id'],
            "model_id" => $Car['model_id'],
            "reg_number" => $Car['reg_number'],
            "color" => $Car['color'],
            "image_path" => $Car['image_path'],
            "description" => $Car['description'],
            "year" => $Car['year'],
            "fuel_type" => $Car['fuel_type'],
            "drive" => $Car['drive'],
            "mileage" => $Car['mileage'],
            "price" => $Car['price'],
        );

        return response()->json($response);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            "reg_number" => 'required|unique:dbProj_cars,reg_number,' . $id,
        ]);

        if ($validator->passes()) {

            $Car = Cars::findOrFail($id);
            $input = $request->all();

            if ($Car->fill($input)->save()) {
                return response()->json(['status' => '00', 'message' => 'Updated ' . $Car->reg_number . ' Successfully']);
            } else {
                return response()->json(['status' => '01', 'error' => 'Error Updating ' . $Car->reg_number . '.']);
            }
        } else {
            return response()->json(['status' => '01', 'message' => $this->returnMessageString($validator->errors()->getMessages())]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $Car = Cars::findOrFail($id);
        $Car->delete();

        return response()->json(['status' => '00', 'message' => 'Car model has been Deleted Successfully']);
    }

    public function carData()
    {

        $models = Cars::join("dbProj_make", "dbProj_cars.make_id", "=", "dbProj_make.id")
            ->join("dbProj_model", "dbProj_cars.model_id", "=", "dbProj_model.id")
            ->select("dbProj_cars.*", "dbProj_make.name as make", "dbProj_model.name as model")
            ->get();

        return Datatables::of($models)
            ->editColumn('active', function ($model) {
                if ($model->active == 1) {
                    return '
                    <a href="#" type="button" class="btn btn-success btn-xs deactivate" data-toggle="modal" data-id="' . $model->id . '"
                    data-name="' . $model->name . '" title="Deactivate Car" data-target="#deactivateModal" > Active </a>
                    ';
                } else {
                    return '
                    <a href="#" type="button" class="btn btn-warning btn-xs activate" data-toggle="modal" data-id="' . $model->id . '"
                data-name="' . $model->name . '" title="Activate Car" data-target="#activateModal" > Inactive </a>
                    ';
                }
            })
            ->editColumn('actions', function ($model) {
                return '
                 <a href="#" type="button" class="btn btn-primary btn-xs edit" data-toggle="modal" data-id="' . $model->id . '"
                data-name="' . $model->name . '" title="Edit Car" data-target="#editModal" > <i class="fa fa-edit"></i></a>

                <a href="#" type="button" class="btn btn-danger btn-xs delete"  data-toggle="modal" data-id="' . $model->id . '"
                 data-name="' . $model->name . '" title="Delete Car" data-target="#deleteModal"> <i class="fa fa-trash-o"></i> </a>

                ';
            })
            ->rawColumns(['active', 'actions'])
            ->make(true);

    }


    public function activate(Request $request)
    {

        $id = $request->input('id');
        Cars::where('id', $id)
            ->update(['active' => 1]);
        return response()->json(['status' => '00', 'message' => 'Car has been Activated Successfully']);

    }

    public function deactivate(Request $request)
    {

        $id = $request->input('id');
        Cars::where('id', $id)
            ->update(['active' => 2]);
        return response()->json(['status' => '00', 'message' => 'Car has been deactivated Successfully']);

    }
}
