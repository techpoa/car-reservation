<?php

namespace App\Http\Controllers;

use App\Accessories;
use App\Cars;
use Illuminate\Http\Request;

class PageController extends Controller
{
    //
    public function index()
    {
        return view("index");
    }

    public function about()
    {
        return view("about");
    }

    public function cars()
    {
        $cars = Cars::join("dbProj_make","dbProj_cars.make_id","=","dbProj_make.id")
            ->join("dbProj_model","dbProj_cars.model_id","=","dbProj_cars.id")
            ->where("dbProj_cars.active","=",1)
            ->select("dbProj_cars.*","dbProj_make.name as make","dbProj_model.name as model")
            ->get();
        return view("cars.index",compact("cars"));
    }

    public function rentCar($id)
    {
        $car = Cars::join("dbProj_make","dbProj_cars.make_id","=","dbProj_make.id")
            ->join("dbProj_model","dbProj_cars.model_id","=","dbProj_cars.id")
            ->where("dbProj_cars.active","=",1)
            ->select("dbProj_cars.*","dbProj_make.name as make","dbProj_model.name as model")
            ->firstOrFail();

        $cars = Cars::where('id', '!=', $id)
            ->where('active','=',1)
            ->get();

        $accessories = Accessories::where("active","=",1)->get();

        return view("cars.details",compact("car","cars","accessories"));



    }

    public function  contact()
    {
        return view("contact");
    }

    public function  postContact()
    {
//        return view("contact");
    }

}
