<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required:dbProj_users',
            'username' => 'required|unique:dbProj_users',
        ]);
        //
        if ($validator->passes())  //
        {
            $User = new User();

            $User->first_name = $request->input('first_name');
            $User->last_name = $request->input('last_name');
            $User->email = $request->input('email');
            $User->phone = $request->input('phone');
            $User->username = $request->input('username');
            $User->password = Hash::make($request->input('password'));
            $User->active = 1;
            $User->created_at = Carbon::now();

            if ($User->save()) {
                return response()->json(['status' => '00', 'message' => 'Created ' . $User->first_name . ' successfully']);

            } else {
                return response()->json(['status' => '01', 'message' => 'Error when creating user']);
            }
        } else {

            return response()->json(['status' => '01', 'message' => $this->returnMessageString($validator->errors()->getMessages())]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $User = User::find($id);

        $response = array(
            "status" => "00",
            "id" => $User['id'],
            "first_name" => $User['first_name'],
            "last_name" => $User['last_name'],
            "email" => $User['email'],
            "phone" => $User['phone'],
            "username" => $User['username'],
        );

        return response()->json($response);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'first_name' => 'required:dbProj_users',
            'username' => 'required|unique:dbProj_users,username,' . $id,
        ]);
        //
        if ($validator->passes()) {
            $User = User::findOrFail($id);
            $input = $request->all();

            if ($request->input('password')) {
                $User->email = Hash::make($request->input('password'));
            }

            if ($User->fill($input)->save()) {
                return response()->json(['status' => '00', 'message' => 'Updated ' . $User->first_name . ' Successfully']);
            } else {
                return response()->json(['status' => '01', 'error' => 'Error Updating ' . $User->first_name . '.']);
            }
        } else {
            return response()->json(['status' => '01', 'message' => $this->returnMessageString($validator->errors()->getMessages())]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $User = User::findOrFail($id);
        $User->delete();

        return response()->json(['status' => '00', 'message' => 'User make has been Deleted Successfully']);

    }


    public function userData()
    {

        $users = User::all();

        return Datatables::of($users)
            ->editColumn('active', function ($user) {
                if ($user->active == 1) {
                    return '
                    <a href="#" type="button" class="btn btn-success btn-xs deactivate" data-toggle="modal" data-id="' . $user->id . '"
                    data-name="' . $user->name . '" title="Deactivate User" data-target="#deactivateModal" > Active </a>
                    ';
                } else {
                    return '
                    <a href="#" type="button" class="btn btn-warning btn-xs activate" data-toggle="modal" data-id="' . $user->id . '"
                data-name="' . $user->name . '" title="Activate User" data-target="#activateModal" > Inactive </a>
                    ';
                }
            })
            ->editColumn('actions', function ($user) {
                return '
                 <a href="#" type="button" class="btn btn-primary btn-xs edit" data-toggle="modal" data-id="' . $user->id . '"
                data-name="' . $user->name . '" title="Edit User" data-target="#editModal" > <i class="fa fa-edit"></i></a>

                <a href="#" type="button" class="btn btn-danger btn-xs delete"  data-toggle="modal" data-id="' . $user->id . '"
                 data-name="' . $user->name . '" title="Delete User" data-target="#deleteModal"> <i class="fa fa-trash-o"></i> </a>

                ';
            })
            ->rawColumns(['active', 'actions'])
            ->make(true);

    }


    public function activate(Request $request)
    {

        $id = $request->input('id');

        User::where('id', $id)
            ->update(['active' => 1]);

        return response()->json(['status' => '00', 'message' => 'User has been Activated Successfully']);

    }

    public function deactivate(Request $request)
    {

        $id = $request->input('id');

        User::where('id', $id)
            ->update(['active' => 2]);

        return response()->json(['status' => '00', 'message' => 'User has been deactivated Successfully']);

    }

}
