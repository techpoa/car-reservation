<?php

namespace App\Http\Controllers;

use App\Accessories;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class AccessoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:dbProj_accessories',
        ]);
        //
        if($validator -> passes())  //
        {
            $Accessories = new Accessories();

            $Accessories->name = $request->input('name');
            $Accessories->price = $request->input('price');
            $Accessories->description = $request->input('description');
            $Accessories->active = 1;
            $Accessories->created_at = Carbon::now();

            if ($Accessories->save())
            {
                return response()->json(['status' => '00', 'message' => 'Created '.$Accessories -> name .' accessory successfully']);

            }
            else{
                return response()->json(['status' => '01', 'message' => 'Error when creating accessory']);
            }
        }
        else{

            return response()->json(['status' => '01', 'message' => $this -> returnMessageString($validator->errors()->getMessages())]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $Accessories = Accessories::find($id);

        $response = array(
            "status" => "00",
            "id" => $Accessories['id'],
            "name" => $Accessories['name'],
            "price" => $Accessories['price'],
            "description" => $Accessories['description'],
        );

        return response()->json($response);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:dbProj_accessories,name,'.$id,
        ]);
        //
        if($validator -> passes())
        {
            $Accessories = Accessories::findOrFail($id);
            $input = $request->all();

            if ($Accessories->fill($input)->save())
            {
                return response()->json(['status' => '00', 'message' => 'Updated '. $Accessories -> name.' accessory Successfully']);
            }
            else {
                return response()->json(['status' => '01', 'error' => 'Error Updating '. $Accessories -> name.' accessory Successfully']);
            }
        }
        else{
            return response()->json(['status' => '01', 'message' => $this -> returnMessageString($validator->errors()->getMessages())]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
            $Accessories = Accessories::findOrFail($id);
            $Accessories->delete();

            return response()->json(['status' => '00', 'message' => 'Car accessory has been Deleted Successfully']);

    }

    public function accessoryData()
    {

        $accessories = Accessories::all();

        return Datatables::of($accessories)
            ->editColumn('active', function ($accessory) {
                if ($accessory->active == 1)
                {
                    return '
                    <a href="#" type="button" class="btn btn-success btn-xs deactivate" data-toggle="modal" data-id="'.$accessory->id .'"
                    data-name="'.$accessory->name .'" title="Deactivate Accessory" data-target="#deactivateModal" > Active </a>
                    ';
                }
                else{
                    return '
                    <a href="#" type="button" class="btn btn-warning btn-xs activate" data-toggle="modal" data-id="'.$accessory->id .'"
                data-name="'.$accessory->name .'" title="Activate Accessory" data-target="#activateModal" > Inactive </a>
                    ';
                }
            })
            ->editColumn('actions', function ($accessory) {
                return '
                 <a href="#" type="button" class="btn btn-primary btn-xs edit" data-toggle="modal" data-id="'.$accessory->id .'"
                data-name="'.$accessory->name .'" title="Edit Accessory" data-target="#editModal" > <i class="fa fa-edit"></i></a>

                <a href="#" type="button" class="btn btn-danger btn-xs delete"  data-toggle="modal" data-id="'.$accessory->id .'"
                 data-name="'.$accessory->name .'" title="Delete Accessory" data-target="#deleteModal"> <i class="fa fa-trash-o"></i> </a>

                ';
            })
            ->rawColumns(['active','actions'])
            ->make(true);

    }


    public function activate(Request $request){

        $id = $request ->input('id');

        Accessories::where('id', $id)
            ->update(['active' => 1]);

        return response()->json(['status' => '00', 'message' => 'Accessory has been Activated Successfully']);

    }

    public function deactivate(Request $request){

        $id = $request ->input('id');

        Accessories::where('id', $id)
            ->update(['active' => 2]);

        return response()->json(['status' => '00', 'message' => 'Accessory has been deactivated Successfully']);

    }
}
