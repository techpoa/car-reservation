<?php

namespace App\Http\Controllers;

use App\Accessories;
use App\Cars;
use App\Customers;
use App\Make;
use App\Models;
use App\Orders;
use App\Payments;
use App\User;
use Faker\Provider\Payment;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $users = User::where('active', '=', 1)->count();
        $accessories = Accessories::where('active', '=', 1)->count();
        $cars = Cars::where('active', '=', 1)->count();
        $customers = Customers::where('active', '=', 1)->count();
        $orders = Orders::where('active', '=', 1)->count();
        $payments = Payments::where('status', '=', 1)->sum('amount');

        return view('admin.index')
            ->with('users', $users)
            ->with('accessories', $accessories)
            ->with('cars', $cars)
            ->with('customers', $customers)
            ->with('orders', $orders)
            ->with('payments', $payments);

    }

    public function user()
    {
        return view('admin.users.index');
    }

    public function make()
    {
        return view('admin.make.index');
    }

    public function model()
    {
        $cars = Make::where('active','=',1)->get();
        return view('admin.model.index',compact("cars"));
    }

    public function cars()
    {
        $cars = Make::where('active','=',1)->get();
        $models = Models::where('active','=',1)->get();
        return view('admin.cars.index',compact("cars","models"));
    }

    public function orders()
    {
        return view('admin.orders.index');
    }

    public function accessories()
    {
        return view('admin.accessories.index');
    }

    public function customers()
    {
        return view('admin.customers.index');
    }

    public function payments()
    {
        return view('admin.payments.index');
    }

    public function reports()
    {
        return view('admin.reports.index');
    }
}
