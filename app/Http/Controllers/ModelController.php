<?php

namespace App\Http\Controllers;

use App\Cars;
use App\Models;
use App\Traits\FormatAjaxValidationMessages;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class ModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:dbProj_model',
        ]);
        //
        if($validator -> passes())
        {
            $Model = new Models();

            $Model->make_id = $request->input('make');
            $Model->name = $request->input('name');
            $Model->description = $request->input('description');
            $Model->active = 1;
            $Model->created_at = Carbon::now();

            if ($Model->save())
            {
                return response()->json(['status' => '00', 'message' => 'Created '.$Model -> name .' model successfully']);

            }
            else{
                return response()->json(['status' => '01', 'message' => 'Error when creating model']);
            }
        }
        else{

            return response()->json(['status' => '01', 'message' => $this -> returnMessageString($validator->errors()->getMessages())]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $Model = Models::find($id);

        $response = array(
            "status" => "00",
            "id" => $Model['id'],
            "make_id" => $Model['make_id'],
            "name" => $Model['name'],
            "description" => $Model['description'],
        );

        return response()->json($response);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:dbProj_model',
        ]);
        //
        if($validator -> passes())
        {
            $Model = Models::findOrFail($id);
            $input = $request->all();

            if ($Model->fill($input)->save())
            {
                return response()->json(['status' => '00', 'message' => 'Updated '. $Model -> name.' model Successfully']);
            }
            else {
                return response()->json(['status' => '01', 'error' => 'Error Updating '. $Model -> name.' model Successfully']);
            }
        }
        else{
            return response()->json(['status' => '01', 'message' => $this -> returnMessageString($validator->errors()->getMessages())]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $Model = Models::find($id);

        $cars_model = Cars::where('model_id', 'like', $Model -> id)
            ->get();

        if ($cars_model->isEmpty())
        {
            $Model = Models::findOrFail($id);
            $Model->delete();

            return response()->json(['status' => '00', 'message' => 'Car model has been Deleted Successfully']);
        }
        else{
            return response()->json(['status' => '01', 'message' => 'Car model has an associated listed car -- Cannot be deleted']);
        }
    }

    public function modelData()
    {

        $models = Models::join('dbProj_make', 'dbProj_model.make_id', '=', 'dbProj_make.id')
            ->select(
                'dbProj_model.*',
                'dbProj_make.name as make'
            )
            ->get();

        return Datatables::of($models)
            ->editColumn('active', function ($model) {
                if ($model->active == 1)
                {
                    return '
                    <a href="#" type="button" class="btn btn-success btn-xs deactivate" data-toggle="modal" data-id="'.$model->id .'"
                    data-name="'.$model->name .'" title="Deactivate Model" data-target="#deactivateModal" > Active </a>
                    ';
                }
                else{
                    return '
                    <a href="#" type="button" class="btn btn-warning btn-xs activate" data-toggle="modal" data-id="'.$model->id .'"
                data-name="'.$model->name .'" title="Activate Model" data-target="#activateModal" > Inactive </a>
                    ';
                }
            })
            ->editColumn('actions', function ($model) {
                return '
                 <a href="#" type="button" class="btn btn-primary btn-xs edit" data-toggle="modal" data-id="'.$model->id .'"
                data-name="'.$model->name .'" title="Edit Model" data-target="#editModal" > <i class="fa fa-edit"></i></a>

                <a href="#" type="button" class="btn btn-danger btn-xs delete"  data-toggle="modal" data-id="'.$model->id .'"
                 data-name="'.$model->name .'" title="Delete Model" data-target="#deleteModal"> <i class="fa fa-trash-o"></i> </a>

                ';
            })
            ->rawColumns(['active','actions'])
            ->make(true);

    }


    public function activate(Request $request){

        $id = $request ->input('id');

        Models::where('id', $id)
            ->update(['active' => 1]);

        return response()->json(['status' => '00', 'message' => 'Model has been Activated Successfully']);

    }

    public function deactivate(Request $request){

        $id = $request ->input('id');

        Models::where('id', $id)
            ->update(['active' => 2]);

        return response()->json(['status' => '00', 'message' => 'Model has been deactivated Successfully']);

    }

}
