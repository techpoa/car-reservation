<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    //
    protected $table = 'dbProj_orders';

    protected $fillable = [
        'order_number', 'order_price', 'pickup_location', 'pickup_date', 'pickup_hour', 'active', 'created_at',
    ];
}
