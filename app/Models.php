<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Models extends Model
{
    //
    protected $table = 'dbProj_model';

    protected $fillable = [
        'make_id',  'name', 'description', 'active', 'created_at',
    ];
}
