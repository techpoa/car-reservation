<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{
    //
    protected $table = 'dbProj_order_details';

    protected $fillable = [
        'order_id', 'item_id', 'item_name', 'price', 'created_at',
    ];
}
