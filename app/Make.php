<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Make extends Model
{
    //
    protected $table = 'dbProj_make';

    protected $fillable = [
        'name', 'description', 'active', 'created_at',
    ];
}
