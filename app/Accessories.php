<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accessories extends Model
{
    //
    protected $table = 'dbProj_accessories';

    protected $fillable = [
        'name', 'price', 'description', 'active', 'created_at',
    ];

}
