<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    //
    protected $table = 'dbProj_payments';

    protected $fillable = [
        'order_id', 'account_name', 'card_number', 'cvv', 'amount', 'status', 'created_at',
    ];
}
