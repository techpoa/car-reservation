<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index');
Route::get('about', 'PageController@about');
Route::get('cars', 'PageController@cars');
Route::get('cars/rent/{id}', 'PageController@rentCar');
Route::get('contact', 'PageController@contact');
Route::post('contact', 'PageController@postContact');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function () {

    Route::get('admin', 'HomeController@index');
    Route::get('admin/user', 'HomeController@user');
    Route::get('admin/make', 'HomeController@make');
    Route::get('admin/model', 'HomeController@model');
    Route::get('admin/cars', 'HomeController@cars');
    Route::get('admin/orders', 'HomeController@orders');
    Route::get('admin/accessories', 'HomeController@accessories');
    Route::get('admin/customers', 'HomeController@customers');
    Route::get('admin/payments', 'HomeController@payments');
    Route::get('admin/payments', 'HomeController@payments');
    Route::get('admin/reports', 'HomeController@reports');

//});


Route::get('userdata', 'UserController@userData');
Route::post('user/activate', 'UserController@activate');
Route::post('user/deactivate', 'UserController@deactivate');
Route::resource('user','UserController');

Route::get('makedata', 'MakeController@makeData');
Route::post('make/activate', 'MakeController@activate');
Route::post('make/deactivate', 'MakeController@deactivate');
Route::resource('make','MakeController');

Route::get('modeldata', 'ModelController@modelData');
Route::post('model/activate', 'ModelController@activate');
Route::post('model/deactivate', 'ModelController@deactivate');
Route::resource('model','ModelController');

Route::get('accessorydata', 'AccessoryController@accessoryData');
Route::post('accessory/activate', 'AccessoryController@activate');
Route::post('accessory/deactivate', 'AccessoryController@deactivate');
Route::resource('accessory','AccessoryController');

Route::get('cardata', 'CarController@carData');
Route::post('car/activate', 'CarController@activate');
Route::post('car/deactivate', 'CarController@deactivate');
Route::resource('car','CarController');

Route::get('paymentsdata', 'PaymentsController@paymentsData');
Route::post('payments/activate', 'PaymentsController@activate');
Route::post('payments/deactivate', 'PaymentsController@deactivate');
Route::resource('payments','PaymentsController');
