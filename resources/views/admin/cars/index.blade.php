<?php
/**
 * Created by PhpStorm.
 * User: muturi muraya <muturi.muraya@gmail.com>
 * Date: 03/06/2021
 * Time: 12:36 AM
 * Project car-reservation
 */

?>

@extends("admin.layouts.master")
@section("content")

    <div class="normalheader ">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="#">
                    <div class="clip-header">
                        <i class="fa fa-arrow-up"></i>
                    </div>
                </a>

                <div id="hbreadcrumb" class="pull-right m-t-lg">
                    <a href="#" class="btn btn-sm btn-primary" id="create" data-toggle="modal"
                       data-target="#createModal"> Create Car</a>
                </div>
                <h2 class="font-light m-b-xs">
                    Cars List
                </h2>
                <ol class="hbreadcrumb breadcrumb">
                    <li><a href="{{ url("admin") }}">Dashboard</a></li>
                    <li>
                        <span>Cars</span>
                    </li>
                    <li class="active">
                        <span>Car</span>
                    </li>
                </ol>
            </div>
        </div>
    </div>

    <div class="content">

        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table id="carTable" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                            <tr>
                                <th>Make</th>
                                <th>Model</th>
                                <th>Color</th>
                                <th>Reg-Number</th>
                                <th>Description</th>
                                <th>Active</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Make</th>
                                <th>Model</th>
                                <th>Color</th>
                                <th>Reg-Number</th>
                                <th>Description</th>
                                <th>Active</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal fade" id="createModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span></button>
                    <h4 class="modal-title"> Create Car </h4>
                </div>
                <form id="frmCreate" role="form" class="form-horizontal" enctype="multipart/form-data">
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <fieldset>
                            <div class="col-md-4">
                                <label for="reg_number"> Reg Number </label>
                                <input type="text" name="reg_number" id="reg_number" class="form-control">
                            </div>
                            <div class="col-md-4">
                                <label for="price"> Price </label>
                                <input type="text" name="price" id="price" class="form-control">
                            </div>
                            <div class="col-md-4">
                                <label for="year"> Year </label>
                                <input type="text" name="year" id="year" class="form-control">
                            </div>
                            <div class="col-md-4">
                                <label for="color"> Color </label>
                                <input type="text" name="color" id="color" class="form-control">
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">Car Make</label>
                                <select class="form-control m-b" name="make" id="make">
                                    @foreach($cars as $car)
                                        <option value="{{ $car ->id }}">{{ $car -> name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">Car Model</label>
                                <select class="form-control m-b" name="model" id="model">
                                    @foreach($models as $model)
                                        <option value="{{ $model ->id }}">{{ $model -> name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-12">
                                <label for="description"> Description </label>
                                <textarea name="description" id="description" class="form-control" rows="3"></textarea>
                            </div>


                            <div class="col-md-6">
                                <label class="control-label">Fuel Type</label>
                                <select class="form-control m-b" name="fuel_type" id="fuel_type">
                                    <option value="Diesel"> Diesel</option>
                                    <option value="Gasoline"> Gasoline</option>
                                </select>
                            </div>

                            <div class="col-md-6">
                                <label class="control-label">Drive Type</label>
                                <select class="form-control m-b" name="drive" id="drive">
                                    <option value="auto"> Auto</option>
                                    <option value="manual"> Manual</option>
                                </select>
                            </div>

                            <div class="col-md-12">
                                <label for="mileage"> Mileage </label>
                                <input type="text" name="mileage" id="mileage" class="form-control">
                            </div>

                            <div class="col-md-12">
                                <label for="image"> Car Picture</label>
                                <div class="well well-sm">
                                    Square Image for this case
                                </div>
                                <input id="image" name="image" type="file" accept="image/*" class="form-control">
                            </div>
                        </fieldset>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="btnCloseCreate" class="btn btn-white" data-dismiss="modal">Close
                        </button>
                        <button type="submit" class="btn btn-primary">Save Details</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal inmodal fade" id="editModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span></button>
                    <h4 class="modal-title"> Edit Car Details </h4>
                </div>
                <div class="modal-body">
                    <form id="frmEdit" name="frmEdit" role="form" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="col-md-4">
                            <label for="editRegnumber"> Reg Number</label>
                            <input type="text" name="reg_number" id="editRegnumber" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label for="editPrice"> Price </label>
                            <input type="text" name="editPrice" id="editPrice" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label for="editYear"> Year </label>
                            <input type="text" name="editYear" id="editYear" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label for="editColor"> Color</label>
                            <input type="text" name="editColor" id="editColor" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Car Make</label>
                            <select class="form-control m-b" name="editMake" id="editMake">
                                @foreach($cars as $car)
                                    <option value="{{ $car ->id }}">{{ $car -> name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Car Model</label>
                            <select class="form-control m-b" name="editModel" id="editModel">
                                @foreach($models as $model)
                                    <option value="{{ $model ->id }}">{{ $model -> name }}</option>
                                @endforeach
                            </select>
                        </div>


                        <div class="col-md-12">
                            <label for="editDescription"> Description </label>
                            <textarea name="editDescription" id="editDescription" class="form-control"
                                      rows="3"></textarea>
                        </div>


                        <div class="col-md-6">
                            <label class="control-label">Fuel Type</label>
                            <select class="form-control m-b" name="editFuel_type" id="editFuel_type">
                                <option value="Diesel"> Diesel</option>
                                <option value="Gasoline"> Gasoline</option>
                            </select>
                        </div>

                        <div class="col-md-6">
                            <label class="control-label">Drive Type</label>
                            <select class="form-control m-b" name="editDrive" id="editDrive">
                                <option value="auto"> Auto</option>
                                <option value="manual"> Manual</option>
                            </select>
                        </div>

                        <div class="col-md-12">
                            <label for="editMileage"> Mileage </label>
                            <input type="text" name="editMileage" id="editMileage" class="form-control">
                        </div>

                        <input type="hidden" id="carID">

                        <div class="form-group">
                            <input type="hidden" class="form-control required">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnCloseEdit" class="btn btn-white" data-dismiss="modal">Close
                    </button>
                    <button type="submit" id="btnSaveEdit" class="btn btn-primary">Save changes</button>
                </div>

            </div>
        </div>
    </div>

    @include('admin.partials.modals')

    <script type='text/javascript' charset="utf-8">
        $(document).ready(function () {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var entity = "Car";
            var createModal = $('#createModal');
            var frmCreate = document.getElementById("frmCreate");
            var editModal = $('#editModal');
            var frmEdit = document.getElementById("frmEdit");
            var activateModal = $('#activateModal');
            var deactivateModal = $('#deactivateModal');
            var deleteModal = $('#deleteModal');


            var carTable = $('#carTable').DataTable({
                "ajax": '/cardata',
                dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                buttons: [
                    {extend: 'copy', className: 'btn-sm'},
                    {extend: 'csv', title: 'ExampleFile', className: 'btn-sm'},
                    {extend: 'pdf', title: 'ExampleFile', className: 'btn-sm'},
                    {extend: 'print', className: 'btn-sm'}
                ],
                "columns": [
                    {data: 'make', name: 'make'},
                    {data: 'model', name: 'model'},
                    {data: 'color', name: 'color'},
                    {data: 'reg_number', name: 'reg_number'},
                    {data: 'description', name: 'description'},
                    {data: 'active', name: 'active'},
                    {data: 'actions', name: 'actions'}
                ]
            });

            $('#frmCreate').submit(function (e) {

                e.preventDefault();
                toastr.info('Info!', 'Please Wait! Creating Car ...');

                let formData = new FormData(this);

                $.ajax({
                    type: "POST",
                    url: "/car",
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    createModal.modal('hide');
                                    frmCreate.reset();
                                    carTable.ajax.reload(null, false);
                                    toastr.success('Success!', data.message);
                                } else if (data.status === '01') {
                                    carTable.ajax.reload(null, false);
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on("click", ".edit", function () {

                var id = $(this).data('id');
                $.ajax({
                    type: "GET",
                    url: "/car/" + id + "/edit",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    document.getElementById("carID").value = id;
                                    document.getElementById("editMake").value = data.make_id;
                                    document.getElementById("editModel").value = data.model_id;
                                    document.getElementById("editColor").value = data.color;
                                    document.getElementById("editRegnumber").value = data.reg_number;
                                    document.getElementById("editDescription").value = data.description;
                                    document.getElementById("editYear").value = data.year;
                                    document.getElementById("editFuel_type").value = data.fuel_type;
                                    document.getElementById("editDrive").value = data.drive;
                                    document.getElementById("editMileage").value = data.mileage;
                                    document.getElementById("editPrice").value = data.price;
                                    $('#editModal').modal('show');
                                    break;

                                } else if (data.status === '01') {
                                    carTable.ajax.reload(null, false);
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $("#btnSaveEdit").click(function () {
                toastr.info('Info!', 'Please Wait!', 'Saving ' + entity + ' Changes ...');

                var id = $("#carID").val();
                var make_id = $("#editMake").val();
                var model_id = $("#editModel").val();
                var color = $("#editColor").val();
                var reg_number = $("#editRegnumber").val();
                var description = $("#editDescription").val();
                var year = $("#editYear").val();
                var fuel_type = $("#editFuel_type").val();
                var drive = $("#editDrive").val();
                var mileage = $("#editMileage").val();
                var price = $("#editPrice").val();

                $.ajax({
                    type: "PATCH",
                    url: "/car/" + id,
                    data: {
                        id: id,
                        make_id: make_id,
                        model_id: model_id,
                        color: color,
                        reg_number: reg_number,
                        description: description,
                        year: year,
                        fuel_type: fuel_type,
                        drive: drive,
                        mileage: mileage,
                        price: price
                    },
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    editModal.modal('hide');
                                    frmEdit.reset();
                                    carTable.ajax.reload()
                                    toastr.success('Success', data.message);
                                } else if (data.status === '01') {
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            // $('#frmEdit').submit(function (e) {
            //
            //     e.preventDefault();
            //     let formData = new FormData(this);
            //
            //     toastr.info('Info!', 'Please Wait!', 'Saving ' + entity + ' Changes ...');
            //
            //     var id = $("#carID").val();
            //     var make_id = $("#editMake").val();
            //     var model_id = $("#editModel").val();
            //     var color = $("#editColor").val();
            //     var reg_number = $("#editRegnumber").val();
            //     var description = $("#editDescription").val();
            //     var year = $("#editYear").val();
            //     var fuel_type = $("#editFuel_type").val();
            //     var drive = $("#editDrive").val();
            //     var mileage = $("#editMileage").val();
            //
            //     $.ajax({
            //         type: "PATCH",
            //         url: "/car/" + id,
            {{--        _token: "{{ csrf_token() }}",--}}
            //         data: formData,
            //         contentType: false,
            //         processData: false,
            //         success: function (data, status) {
            //             switch (status) {
            //                 case "success":
            //                     if (data.status === '00') {
            //                         editModal.modal('hide');
            //                         frmEdit.reset();
            //                         carTable.ajax.reload()
            //                         toastr.success('Success', data.message);
            //                     } else if (data.status === '01') {
            //                         toastr.error('Error!', data.message);
            //                     }
            //                     break;
            //                 case "failed":
            //                     toastr.error('Error!', data.message);
            //                     break;
            //                 default :
            //                     alert("do nothing");
            //             }
            //         }
            //     });
            // });

            $(document).on('click', 'a.activate', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("activateID").value = id;
                $("#activateTitle").html("Activate " + entity);
                $("#activateNotification").html("Are you sure you want to activate " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnActivate').click(function () {

                var id = $("#activateID").val();
                $.ajax({
                    type: "POST",
                    url: "/car/activate",
                    data: {_token: "{{ csrf_token() }}", id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    activateModal.modal('hide');
                                    carTable.ajax.reload(null, false);
                                    toastr.success('Success', data.message);
                                } else if (data.status === '01') {
                                    carTable.ajax.reload(null, false);
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.deactivate', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("deactivateID").value = id;
                $("#deactivateTitle").html("Deactivate " + entity);
                $("#deactivateNotification").html("Are you sure you want to deactivate " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnDeactivate').click(function () {

                var id = $("#deactivateID").val();
                $.ajax({
                    type: "POST",
                    url: "/car/deactivate",
                    data: {_token: "{{ csrf_token() }}", id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    deactivateModal.modal('hide');
                                    carTable.ajax.reload(null, false);
                                    toastr.success('Success', data.message);
                                } else if (data.status === '01') {
                                    carTable.ajax.reload(null, false);
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.delete', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("deleteID").value = id;
                $("#deleteTitle").html("Delete " + entity);
                $("#deleteNotification").html("Are you sure you want to delete " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnDelete').click(function () {

                var id = $("#deleteID").val();
                $.ajax({
                    type: "DELETE",
                    url: "/car/" + id,
                    data: {_token: "{{ csrf_token() }}", id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    deleteModal.modal('hide');
                                    carTable.ajax.reload(null, false);
                                    toastr.success('Success', data.message);
                                } else if (data.status === '01') {
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });
        });
    </script>


@endsection

