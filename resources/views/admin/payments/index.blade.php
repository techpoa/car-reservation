<?php
/**
 * Created by PhpStorm.
 * User: muturi muraya <muturi.muraya@gmail.com>
 * Date: 03/06/2021
 * Time: 11:46 PM
 * Project car-reservation
 */


?>

@extends("admin.layouts.master")
@section("content")

    <div class="normalheader ">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="datatables.html">
                    <div class="clip-header">
                        <i class="fa fa-arrow-up"></i>
                    </div>
                </a>

                <div id="hbreadcrumb" class="pull-right m-t-lg">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="i{{ url("") }}">Dashboard</a></li>
                        <li>
                            <span>Tables</span>
                        </li>
                        <li class="active">
                            <span>Admin</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Payments
                </h2>
            </div>
        </div>
    </div>

    <div class="content">

        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                    </div>

                    <div class="panel-body">
                        <table id="paymentsTable" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                            <tr>
                                <th>Order NO</th>
                                <th>Card Name</th>
                                <th>Card number</th>
                                <th>Exp date</th>
                                <th>CVV</th>
                                <th>Amount</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Order NO</th>
                                <th>Card Name</th>
                                <th>Card number</th>
                                <th>Exp date</th>
                                <th>CVV</th>
                                <th>Amount</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <script type='text/javascript' charset="utf-8">
        $(document).ready(function () {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var entity = "Payments";

            var paymentsTable = $('#paymentsTable').DataTable({
                "ajax": '/paymentsdata',
                dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                buttons: [
                    {extend: 'copy', className: 'btn-sm'},
                    {extend: 'csv', title: 'ExampleFile', className: 'btn-sm'},
                    {extend: 'pdf', title: 'ExampleFile', className: 'btn-sm'},
                    {extend: 'print', className: 'btn-sm'}
                ],
                "columns": [
                    {data: 'order_number', name: 'order_number'},
                    {data: 'card_name', name: 'card_name'},
                    {data: 'card_number', name: 'card_number'},
                    {data: 'exp_date', name: 'exp_date'},
                    {data: 'cvv', name: 'cvv'},
                    {data: 'amount', name: 'amount'},
                    {data: 'status', name: 'status'},
                    {data: 'actions', name: 'actions'}
                ]
            });

        });
    </script>

@endsection
