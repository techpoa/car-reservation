<?php
/**
 * Created by PhpStorm.
 * User: muturi muraya <muturi.muraya@gmail.com>
 * Date: 02/06/2021
 * Time: 11:23 PM
 * Project car-reservation
 */

?>


    <!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Page title -->
    <title>HOMER | WebApp admin theme</title>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

    <!-- Vendor styles -->
    <link rel="stylesheet" href="{{ asset("vendor/fontawesome/css/font-awesome.css") }}"/>
    <link rel="stylesheet" href="{{ asset("vendor/metisMenu/dist/metisMenu.css") }}"/>
    <link rel="stylesheet" href="{{ asset("vendor/animate.css/animate.css") }}"/>
    <link rel="stylesheet" href="{{ asset("vendor/bootstrap/dist/css/bootstrap.css") }}"/>
    <link rel="stylesheet" href="{{ asset("vendor/datatables.net-bs/css/dataTables.bootstrap.min.css") }}"/>

    <!-- App styles -->
    <link rel="stylesheet" href="{{ asset("fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css") }}"/>
    <link rel="stylesheet" href="{{ asset("fonts/pe-icon-7-stroke/css/helper.css") }}"/>
    <link rel="stylesheet" href="{{ asset("styles/style.css") }}">
    <link rel="stylesheet" href="{{ asset("vendor/toastr/build/toastr.min.css") }}"/>

    <!-- Vendor scripts -->
    <script src="{{ asset("vendor/jquery/dist/jquery.min.js") }}"></script>
    <script src="{{ asset("vendor/jquery-ui/jquery-ui.min.js") }}"></script>
    <script src="{{ asset("vendor/slimScroll/jquery.slimscroll.min.js") }}"></script>
    <script src="{{ asset("vendor/bootstrap/dist/js/bootstrap.min.js") }}"></script>
    <script src="{{ asset("vendor/metisMenu/dist/metisMenu.min.js") }}"></script>
    <script src="{{ asset("vendor/iCheck/icheck.min.js") }}"></script>
    <script src="{{ asset("vendor/sparkline/index.js") }}"></script>
    <!-- DataTables -->
    <script src="{{ asset("vendor/datatables/media/js/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("vendor/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
    <!-- DataTables buttons scripts -->
    <script src="{{ asset("vendor/pdfmake/build/pdfmake.min.js") }}"></script>
    <script src="{{ asset("vendor/pdfmake/build/vfs_fonts.js") }}"></script>
    <script src="{{ asset("vendor/datatables.net-buttons/js/buttons.html5.min.js") }}"></script>
    <script src="{{ asset("vendor/datatables.net-buttons/js/buttons.print.min.js") }}"></script>
    <script src="{{ asset("vendor/datatables.net-buttons/js/dataTables.buttons.min.js") }}"></script>
    <script src="{{ asset("vendor/datatables.net-buttons-bs/js/buttons.bootstrap.min.js") }}"></script>
    <script src="{{ asset("vendor/toastr/build/toastr.min.js") }}"></script>
    <script src="{{ asset("vendor/chartjs/Chart.min.js") }}"></script>
    <!-- App scripts -->
    <script src="{{ asset("scripts/homer.js") }}"></script>


</head>
<body class="fixed-navbar sidebar-scroll">

<!-- Simple splash screen-->
<div class="splash">
    <div class="color-line"></div>
    <div class="splash-title"><h1>Homer - Responsive Admin Theme</h1>
        <p>Special Admin Theme for small and medium webapp with very clean and aesthetic style and feel. </p>
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>
</div>
<!--[if lt IE 7]>
<![endif]-->

@include("admin.partials.header")

@include("admin.partials.sidemenu")

<!-- Main Wrapper -->
<div id="wrapper">

    @yield("content")

    @include("admin.partials.footer")

</div>

</body>
</html>
