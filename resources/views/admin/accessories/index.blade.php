<?php
/**
 * Created by PhpStorm.
 * User: muturi muraya <muturi.muraya@gmail.com>
 * Date: 03/06/2021
 * Time: 11:38 PM
 * Project car-reservation
 */

?>

@extends("admin.layouts.master")
@section("content")

    <div class="normalheader ">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="#">
                    <div class="clip-header">
                        <i class="fa fa-arrow-up"></i>
                    </div>
                </a>

                <div id="hbreadcrumb" class="pull-right m-t-lg">
                    <a href="#" class="btn btn-sm btn-primary" id="create" data-toggle="modal"
                       data-target="#createModal"> Create Accessory</a>
                </div>
                <h2 class="font-light m-b-xs">
                    Car Accessories List
                </h2>
                <ol class="hbreadcrumb breadcrumb">
                    <li><a href="{{ url("admin") }}">Dashboard</a></li>
                    <li>
                        <span>Cars</span>
                    </li>
                    <li class="active">
                        <span>Accessories</span>
                    </li>
                </ol>
            </div>
        </div>
    </div>

    <div class="content">

        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table id="accessoriesTable" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Description</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Description</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <div class="modal inmodal fade" id="createModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span></button>
                    <h4 class="modal-title"> Create Car Accessory </h4>
                </div>
                <div class="modal-body">
                    <form id="frmCreate" role="form" class="form-horizontal">
                        {{ csrf_field() }}
                        <fieldset>
                            <div class="form-group">
                                <label for="name"> Name </label>
                                <input type="text" name="name" id="name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="price"> Price </label>
                                <input type="text" name="price" id="price" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="description"> Description </label>
                                <textarea name="description" id="description" class="form-control" rows="5"></textarea>
                            </div>
                        </fieldset>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnCloseCreate" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" id="btnCreate" class="btn btn-primary">Save Details</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal fade" id="editModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span></button>
                    <h4 class="modal-title"> Edit Accessory Details </h4>
                </div>
                <div class="modal-body">
                    <form id="frmEdit" role="form" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="editName"> Name</label>
                            <input type="text" name="editName" id="editName" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="editPrice"> Price </label>
                            <input type="text" name="editPrice" id="editPrice" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="editDescription"> Description </label>
                            <textarea name="editDescription" id="editDescription" class="form-control"
                                      rows="5"></textarea>
                        </div>

                        <input type="hidden" id="accessoryID">

                        <div class="form-group">
                            <input type="hidden" class="form-control required">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnCloseEdit" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" id="btnSaveEdit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    @include('admin.partials.modals')

    <script type='text/javascript' charset="utf-8">
        $(document).ready(function () {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var entity = "Accessory";
            var createModal = $('#createModal');
            var frmCreate = document.getElementById("frmCreate");
            var editModal = $('#editModal');
            var frmEdit = document.getElementById("frmEdit");
            var activateModal = $('#activateModal');
            var deactivateModal = $('#deactivateModal');
            var deleteModal = $('#deleteModal');


            var accessoryTable = $('#accessoriesTable').DataTable({
                "ajax": '/accessorydata',
                dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                buttons: [
                    {extend: 'copy', className: 'btn-sm'},
                    {extend: 'csv', title: 'ExampleFile', className: 'btn-sm'},
                    {extend: 'pdf', title: 'ExampleFile', className: 'btn-sm'},
                    {extend: 'print', className: 'btn-sm'}
                ],
                "columns": [
                    {data: 'name', name: 'name'},
                    {data: 'price', name: 'price'},
                    {data: 'description', name: 'description'},
                    {data: 'active', name: 'active'},
                    {data: 'actions', name: 'actions'}
                ]
            });

            $("#btnCreate").click(function () {
                toastr.info('Info!', 'Please Wait! Creating Make ...');

                var frm = $('#frmCreate');
                var data = frm.serialize();
                $.ajax({
                    type: "POST",
                    url: "/accessory",
                    data: data,
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    createModal.modal('hide');
                                    frmCreate.reset();
                                    accessoryTable.ajax.reload( null, false );
                                    toastr.success('Success!', data.message);
                                } else if (data.status === '01') {
                                    accessoryTable.ajax.reload( null, false );
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on("click", ".edit", function () {

                var id = $(this).data('id');
                $.ajax({
                    type: "GET",
                    url: "/accessory/"+id+"/edit",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    document.getElementById("accessoryID").value = id;
                                    document.getElementById("editName").value = data.name;
                                    document.getElementById("editPrice").value = data.price;
                                    document.getElementById("editDescription").value = data.description;
                                    $('#editModal').modal('show');
                                    break;

                                } else if (data.status === '01') {
                                    accessoryTable.ajax.reload( null, false );
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $("#btnSaveEdit").click(function () {
                toastr.info('Info!', 'Please Wait!', 'Saving '+ entity +' Changes ...');

                var name = $("#editName").val();
                var price = $("#editPrice").val();
                var description = $("#editDescription").val();
                var id = $("#accessoryID").val();
                $.ajax({
                    type: "PATCH",
                    url: "/accessory/"+id,
                    data: {
                        _token: "{{ csrf_token() }}",
                        id: id,
                        name: name,
                        price: price,
                        description: description
                    },
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    editModal.modal('hide');
                                    frmEdit.reset();
                                    accessoryTable.ajax.reload()
                                    toastr.success('Success', data.message);
                                } else if (data.status === '01') {
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.activate', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("activateID").value = id;
                $("#activateTitle").html("Activate " + entity);
                $("#activateNotification").html("Are you sure you want to activate " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnActivate').click(function () {

                var id = $("#activateID").val();
                $.ajax({
                    type: "POST",
                    url: "/accessory/activate",
                    data: { _token: "{{ csrf_token() }}",id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    activateModal.modal('hide');
                                    accessoryTable.ajax.reload(null, false);
                                    toastr.success('Success', data.message);
                                } else if (data.status === '01') {
                                    accessoryTable.ajax.reload(null, false);
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.deactivate', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("deactivateID").value = id;
                $("#deactivateTitle").html("Deactivate " + entity);
                $("#deactivateNotification").html("Are you sure you want to deactivate " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnDeactivate').click(function () {

                var id = $("#deactivateID").val();
                $.ajax({
                    type: "POST",
                    url: "/accessory/deactivate",
                    data: { _token: "{{ csrf_token() }}",id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    deactivateModal.modal('hide');
                                    accessoryTable.ajax.reload(null, false);
                                    toastr.success('Success', data.message);
                                } else if (data.status === '01') {
                                    accessoryTable.ajax.reload(null, false);
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.delete', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("deleteID").value = id;
                $("#deleteTitle").html("Delete " + entity);
                $("#deleteNotification").html("Are you sure you want to delete " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnDelete').click(function () {

                var id = $("#deleteID").val();
                $.ajax({
                    type: "DELETE",
                    url: "/accessory/" + id,
                    data: { _token: "{{ csrf_token() }}",id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    deleteModal.modal('hide');
                                    accessoryTable.ajax.reload(null, false);
                                    toastr.success('Success', data.message);
                                } else if (data.status === '01') {
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

        });
    </script>

@endsection
