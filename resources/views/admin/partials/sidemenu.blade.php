<?php
/**
 * Created by PhpStorm.
 * User: muturi muraya <muturi.muraya@gmail.com>
 * Date: 02/06/2021
 * Time: 11:23 PM
 * Project car-reservation
 */

?>


<!-- Navigation -->
<aside id="menu">
    <div id="navigation">
        <div class="profile-picture">
            <a href="{{ url("admin") }}">
                <img src="{{ asset("images/profile.jpg") }}" class="img-circle m-b" alt="logo">
            </a>

            <div class="stats-label text-color">
                <span class="font-extra-bold font-uppercase"> Sample User </span>

                <div class="dropdown">

                    <a class="dropdown-toggle" href="{{ url("") }}" data-toggle="dropdown">
                        <small class="text-muted">Founder of App <b class="caret"></b></small>
                    </a>

                </div>

                <div id="sparkline1" class="small-chart m-t-sm"></div>
                <div>
                    <small class="text-muted">Your income from the last year in sales product X.</small>
                </div>
            </div>
        </div>

        <ul class="nav" id="side-menu">
            <li class="active">
                <a href="{{ url("admin") }}"> <span class="nav-label">Dashboard</span> </a>
            </li>
            <li class="active">
                <a href="{{ url("admin/user") }}"> <span class="nav-label">Users</span> </a>
            </li>
            <li>
                <a href="#"><span class="nav-label">Cars</span><span class="fa arrow"></span> </a>
                <ul class="nav nav-second-level">
                    <li><a href="{{ url("admin/make") }}">Make</a></li>
                    <li><a href="{{ url("admin/model") }}">Model</a></li>
                    <li><a href="{{ url("admin/accessories") }}">Accessories</a></li>
                    <li><a href="{{ url("admin/cars") }}">Cars</a></li>
                </ul>
            </li>
            <li>
                <a href="{{ url("admin/customers") }}"> <span class="nav-label">Customers</span> </a>
            </li>
            <li>
                <a href="{{ url("admin/orders") }}"> <span class="nav-label">Orders</span> </a>
            </li>
            <li>
                <a href="{{ url("admin/payments") }}"> <span class="nav-label">Payments</span> </a>
            </li>
            <li>
                <a href="{{ url("admin/reports") }}"> <span class="nav-label">Reports</span> </a>
            </li>
        </ul>
    </div>
</aside>
