<?php
/**
 * Created by PhpStorm.
 * User: muturi muraya <muturi.muraya@gmail.com>
 * Date: 05/06/2021
 * Time: 11:16 AM
 * Project car-reservation
 */
?>


<div class="modal inmodal fade" id="activateModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="activateTitle"></h4>
                </div>
                <div class="modal-body">
                    <div id="activateNotification"></div>
                    <input type="hidden" id="activateID">
                </div>
                <div class="modal-footer">
                    <button type="button" id="closeActivate" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="button" id="btnActivate" class="btn btn-primary">Save Changes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal fade" id="deactivateModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="deactivateTitle"></h4>
                </div>
                <div class="modal-body">

                    <div id="deactivateNotification"></div>
                    <input type="hidden" id="deactivateID">

                </div>

                <div class="modal-footer">
                    <button type="button" id="closeDeactivate" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="button" id="btnDeactivate" class="btn btn-primary">Save Changes</button>
                </div>

            </div>
        </div>
    </div>

    <div class="modal inmodal fade" id="cancelModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="cancelTitle"></h4>
                </div>
                <div class="modal-body">

                    <div id="cancelNotification"></div>
                    <input type="hidden" id="cancelID">

                </div>

                <div class="modal-footer">
                    <button type="button" id="closeCancel" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="button" id="btnCancel" class="btn btn-primary">Cancel</button>
                </div>

            </div>
        </div>
    </div>

    <div class="modal inmodal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="deleteTitle"></h4>
                </div>
                <div class="modal-body">

                    <div id="deleteNotification"></div>
                    <input type="hidden" id="deleteID">

                </div>

                <div class="modal-footer">
                    <button type="button" id="closeDelete" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="button" id="btnDelete" class="btn btn-primary">Delete</button>
                </div>

            </div>
        </div>
    </div>


