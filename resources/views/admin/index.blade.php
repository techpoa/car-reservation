<?php
/**
 * Created by PhpStorm.
 * User: muturi muraya <muturi.muraya@gmail.com>
 * Date: 02/06/2021
 * Time: 11:22 PM
 * Project car-reservation
 */

?>

@extends("admin.layouts.dashboard")
@section("content")

    <div class="content">
        <div class="row">
            <div class="col-lg-12 text-center welcome-message">
                <h2>
                    Welcome to Rent-it
                </h2>
                <p>
                    One Stop <strong> Admin Portal </strong> for car rentals.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2">
                <div class="hpanel">
                    <div class="panel-body text-center h-200">
                        <h1 class="m-xs">{{ $users }}</h1>
                        <h3 class="font-extra-bold no-margins text-success">
                            Users
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="hpanel">
                    <div class="panel-body text-center h-200">
                        <h1 class="m-xs">{{ $accessories }}</h1>
                        <h3 class="font-extra-bold no-margins text-success">
                            Accessories
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="hpanel">
                    <div class="panel-body text-center h-200">
                        <h1 class="m-xs">{{ $cars }}</h1>
                        <h3 class="font-extra-bold no-margins text-success">
                            Cars
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="hpanel">
                    <div class="panel-body text-center h-200">
                        <h1 class="m-xs">{{ $customers }}</h1>
                        <h3 class="font-extra-bold no-margins text-success">
                            Customers
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="hpanel">
                    <div class="panel-body text-center h-200">
                        <h1 class="m-xs">{{ $orders }}</h1>
                        <h3 class="font-extra-bold no-margins text-success">
                            Orders
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="hpanel">
                    <div class="panel-body text-center h-200">
                        <h1 class="m-xs">{{ $payments }}</h1>
                        <h3 class="font-extra-bold no-margins text-success">
                            Payments
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
