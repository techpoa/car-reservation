<?php
/**
 * Created by PhpStorm.
 * User: muturi muraya <muturi.muraya@gmail.com>
 * Date: 03/06/2021
 * Time: 11:50 PM
 * Project car-reservation
 */


?>

@extends("admin.layouts.master")
@section("content")

    <div class="normalheader ">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="#">
                    <div class="clip-header">
                        <i class="fa fa-arrow-up"></i>
                    </div>
                </a>

                <div id="hbreadcrumb" class="pull-right m-t-lg">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="i{{ url("") }}">Dashboard</a></li>
                        <li>
                            <span>Admin</span>
                        </li>
                        <li class="active">
                            <span>Reports</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Reports
                </h2>
            </div>
        </div>
    </div>

    <div class="content">

        <div class="row">
            <div class="col-sm-12 col-lg-7">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        Sales Revenue
                    </div>
                    <div class="panel-body">
                        <div>
                            <canvas id="lineOptions" height="140"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-5">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        Car Reservation Popularity
                    </div>
                    <div class="panel-body">
                        <div>
                            <canvas id="doughnutChart" height="200"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>



    <script>

        $(function () {


            var lineData = {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [

                    {
                        label: "Sales",
                        backgroundColor: 'rgba(98,203,49, 0.5)',
                        pointBorderWidth: 1,
                        pointBackgroundColor: "rgba(98,203,49,1)",
                        pointRadius: 3,
                        pointBorderColor: '#ffffff',
                        borderWidth: 1,
                        data: [16, 32, 18, 26, 42, 33, 44]
                    },
                    {
                        label: "Revenue",
                        backgroundColor: 'rgba(220,220,220,0.5)',
                        borderColor: "rgba(220,220,220,0.7)",
                        pointBorderWidth: 1,
                        pointBackgroundColor: "rgba(220,220,220,1)",
                        pointRadius: 3,
                        pointBorderColor: '#ffffff',
                        borderWidth: 1,
                        data: [22, 44, 67, 43, 76, 45, 12]
                    }
                ]
            };

            var lineOptions = {
                responsive: true
            };

            var ctx = document.getElementById("lineOptions").getContext("2d");
            new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});


            var doughnutData = {
                labels: [
                    "BMW",
                    "Mercedes",
                    "Toyota"
                ],
                datasets: [
                    {
                        data: [20, 120, 100],
                        backgroundColor: [
                            "#62cb31",
                            "#91dc6e",
                            "#a3e186"
                        ],
                        hoverBackgroundColor: [
                            "#57b32c",
                            "#57b32c",
                            "#57b32c"
                        ]
                    }]
            }


            var doughnutOptions = {
                responsive: true
            };

            var ctx = document.getElementById("doughnutChart").getContext("2d");
            new Chart(ctx, {type: 'doughnut', data: doughnutData, options:doughnutOptions});

        });

    </script>

@endsection
