<?php
/**
 * Created by PhpStorm.
 * User: muturi muraya <muturi.muraya@gmail.com>
 * Date: 02/06/2021
 * Time: 11:04 AM
 * Project car-reservation
 */
?>

@extends("layouts.index")
@section("content")

    <!-- CONTENT AREA -->
    <div class="content-area">

        <!-- PAGE -->
        <section class="page-section text-center error-page light">
            <div class="container">
                <h3>404</h3>
                <h2><i class="fa fa-warning"></i> Oops! The Page you requested was not found!</h2>
                <p><a class="btn btn-theme btn-theme-dark" href="{{ url("") }}">Back to Home</a></p>
            </div>
        </section>
        <!-- /PAGE -->

        <!-- PAGE -->
        <section class="page-section contact dark">
            <div class="container">

                <!-- Get in touch -->

                <h2 class="section-title">
                    <small>Feel Free to Say Hello!</small>
                    <span>Get in Touch With Us</span>
                </h2>

                <!-- /Get in touch -->

            </div>
        </section>
        <!-- /PAGE -->

    </div>
    <!-- /CONTENT AREA -->
@endsection
