<?php
/**
 * Created by PhpStorm.
 * User: muturi muraya <muturi.muraya@gmail.com>
 * Date: 02/06/2021
 * Time: 9:39 AM
 * Project car-reservation
 */

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Rent It</title>

    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="shortcut icon" href="assets/ico/favicon.ico">

    <!-- CSS Global -->
    <link href="{{ asset("assets/plugins/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet">
    <link href="{{ asset("assets/plugins/bootstrap-select/css/bootstrap-select.min.css") }}" rel="stylesheet">
    <link href="{{ asset("assets/plugins/fontawesome/css/font-awesome.min.css") }}" rel="stylesheet">
    <link href="{{ asset("assets/plugins/prettyphoto/css/prettyPhoto.css") }}" rel="stylesheet">
    <link href="{{ asset("assets/plugins/owl-carousel2/assets/owl.carousel.min.css") }}" rel="stylesheet">
    <link href="{{ asset("assets/plugins/owl-carousel2/assets/owl.theme.default.min.css") }}" rel="stylesheet">
    <link href="{{ asset("assets/plugins/animate/animate.min.css") }}" rel="stylesheet">
    <link href="{{ asset("assets/plugins/swiper/css/swiper.min.css") }}" rel="stylesheet">
    <link href="{{ asset("assets/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css") }}" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="{{ asset("assets/css/theme.css") }}" rel="stylesheet">
    <link href="{{ asset("assets/css/theme-red-1.css") }}" rel="stylesheet" id="theme-config-link">

    <!-- Head Libs -->
    <script src="{{ asset("assets/plugins/modernizr.custom.js") }}"></script>

    <!--[if lt IE 9]>
    <script src="{{ asset("assets/plugins/iesupport/html5shiv.js") }}"></script>
    <script src="{{ asset("assets/plugins/iesupport/respond.min.js") }}"></script>
    <![endif]-->
</head>
<body id="home" class="wide">

 @include("partials.preloader")

<!-- WRAPPER -->
<div class="wrapper">

  @include("partials.header")

    @yield("content")

    @include("partials.footer")

</div>
<!-- /WRAPPER -->

<!-- JS Global -->
<script src="{{ asset("assets/plugins/jquery/jquery-1.11.1.min.js") }}"></script>
<script src="{{ asset("assets/plugins/bootstrap/js/bootstrap.min.js") }}"></script>
<script src="{{ asset("assets/plugins/bootstrap-select/js/bootstrap-select.min.js") }}"></script>
<script src="{{ asset("assets/plugins/superfish/js/superfish.min.js") }}"></script>
<script src="{{ asset("assets/plugins/prettyphoto/js/jquery.prettyPhoto.js") }}"></script>
<script src="{{ asset("assets/plugins/owl-carousel2/owl.carousel.min.js") }}"></script>
<script src="{{ asset("assets/plugins/jquery.sticky.min.js") }}"></script>
<script src="{{ asset("assets/plugins/jquery.easing.min.js") }}"></script>
<script src="{{ asset("assets/plugins/jquery.smoothscroll.min.js") }}"></script>
<!--<script src="assets/plugins/smooth-scrollbar.min.js"></script>-->
<!--<script src="assets/plugins/wow/wow.min.js"></script>-->
<script>
    // WOW - animated content
    //new WOW().init();
</script>
<script src="{{ asset("assets/plugins/swiper/js/swiper.jquery.min.js") }}"></script>
<script src="{{ asset("assets/plugins/datetimepicker/js/moment-with-locales.min.js") }}"></script>
<script src="{{ asset("assets/plugins/datetimepicker/js/bootstrap-datetimepicker.min.js") }}"></script>

<!-- JS Page Level -->
<script src="{{ asset("assets/js/theme-ajax-mail.js") }}"></script>
<script src="{{ asset("assets/js/theme.js") }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>

<!--[if (gte IE 9)|!(IE)]><!-->
<script src="{{ asset("assets/plugins/jquery.cookie.js") }}"></script>
<!--<![endif]-->

</body>
</html>
