<?php
/**
 * Created by PhpStorm.
 * User: muturi muraya <muturi.muraya@gmail.com>
 * Date: 07/06/2021
 * Time: 6:03 PM
 * Project car-reservation
 */
?>

@extends("layouts.index")
@section("content")

    <!-- CONTENT AREA -->
    <div class="content-area">

        <!-- BREADCRUMBS -->
        <section class="page-section breadcrumbs text-right">
            <div class="container">
                <div class="page-header">
                    <h1>Car Booking</h1>
                </div>
                <ul class="breadcrumb">
                    <li><a href="{{ url("") }}">Home</a></li>
                    <li><a href="{{ url("cars") }}">Cars</a></li>
                    <li class="active"> {{ $car -> make }} {{ $car -> model }}</li>
                </ul>
            </div>
        </section>
        <!-- /BREADCRUMBS -->

        <!-- PAGE WITH SIDEBAR -->
        <section class="page-section with-sidebar sub-page">
            <div class="container">
                <div class="row">
                    <!-- CONTENT -->
                    <div class="col-md-9 content" id="content">

                        <h3 class="block-title alt"><i class="fa fa-angle-down"></i>Car Information</h3>
                        <div class="car-big-card alt">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="owl-carousel img-carousel">
                                        <div class="item">
                                            <div class="owl-carousel img-carousel">
                                                <div class="item">
                                                    <a class="btn btn-zoom" href="{{ asset("/assets/img/preview/cars/car-600x426x1.jpg") }}" data-gal="prettyPhoto">
                                                        <i class="fa fa-arrows-h"></i>
                                                    </a>
                                                    <a href="{{ asset("/assets/img/preview/cars/car-600x426x1.jpg") }}" data-gal="prettyPhoto">
                                                        <img class="img-responsive" src="{{ asset("/assets/img/preview/cars/car-600x426x1.jpg") }}" alt=""/>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="row car-thumbnails">
                                                <div class="col-xs-2 col-sm-2 col-md-3">
                                                    <a href="#" onclick="jQuery('.img-carousel').trigger('to.owl.carousel', [0,300]);">
                                                        <img src="assets/img/preview/cars/car-70x70x1.jpg" alt=""/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="car-details">
                                        <div class="list">
                                            <ul>
                                                <li class="title">
                                                    <h2>{{ $car -> make }} <span> {{ $car -> model }}</span></h2>
                                                </li>
                                                <li>Fuel {{ $car -> fuel_type }} / 1600 cm3 Engine</li>
                                                <li>Under {{ $car -> mileage }} Km</li>
                                                <li>Transmission {{ $car -> drive }}</li>
                                                <li>5 Year service included</li>
                                                <li>Manufacturing Year {{ $car -> year }}</li>
                                            </ul>
                                        </div>
                                        <div class="price">
                                            <strong>{{ $car -> price }}</strong> <span>$/per day</span>
                                            <i class="fa fa-info-circle"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="page-divider half transparent"/>

                        <h3 class="block-title alt"><i class="fa fa-angle-down"></i>Extras & Frees</h3>
                        <form role="form" class="form-extras">

                            <div class="row">

                                <div class="col-md-6">
                                    <div class="left">

                                        @foreach($accessories as $accessory)

                                        <div class="checkbox checkbox-danger">
                                            <input id="checkboxl1" type="checkbox">
                                            <label for="checkboxl1"> {{ $accessory -> name }} <span
                                                    class="pull-right">{{ $accessory -> price }} $ /for a day</span></label>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="right">
                                        <div class="checkbox checkbox-danger">
                                            <input id="checkboxr1" type="checkbox" checked="">
                                            <label for="checkboxr1">Ful Rent a Car Insures <span
                                                    class="pull-right">Free</span></label>
                                        </div>
                                        <div class="checkbox checkbox-danger">
                                            <input id="checkboxr2" type="checkbox" checked="">
                                            <label for="checkboxr2">Wheels and Glass Insures <span class="pull-right">Free</span></label>
                                        </div>
                                        <div class="checkbox checkbox-danger">
                                            <input id="checkboxr3" type="checkbox" checked="">
                                            <label for="checkboxr3">Taking from Airport <span
                                                    class="pull-right">Free</span></label>
                                        </div>
                                        <div class="checkbox checkbox-danger">
                                            <input id="checkboxr4" type="checkbox" checked="">
                                            <label for="checkboxr4">Unlimited Kilometres for ALL cars <span
                                                    class="pull-right">Free</span></label>
                                        </div>
                                        <div class="checkbox checkbox-danger">
                                            <input id="checkboxr5" type="checkbox" checked="">
                                            <label for="checkboxr5">VAT <span class="pull-right">Included</span></label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </form>

                        <h3 class="block-title alt"><i class="fa fa-angle-down"></i>Customer Information</h3>
                        <form action="#" class="form-delivery">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="radio radio-inline">
                                        <input type="radio" id="inlineRadio1" value="Mr" name="radioInline" checked="">
                                        <label for="inlineRadio1">Mr</label>
                                    </div>
                                    <div class="radio radio-inline">
                                        <input type="radio" id="inlineRadio2" value="Ms" name="radioInline">
                                        <label for="inlineRadio2">Ms</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="fd-name" id="fd-name" title="Name is required"
                                               data-toggle="tooltip"
                                               class="form-control alt" type="text" placeholder="Name and Surname:*">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="fd-name" id="fd-email" title="Email is required"
                                               data-toggle="tooltip"
                                               class="form-control alt" type="text" placeholder="Your Email Address:*">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group"><input class="form-control alt" type="text"
                                                                   placeholder="Phone Number:"></div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group"><input class="form-control alt" type="text"
                                                                   placeholder="Cell Phone Number:"></div>
                                </div>
                            </div>
                        </form>

                        <h3 class="block-title alt"><i class="fa fa-angle-down"></i>Payments options</h3>
                        <div class="panel-group payments-options" id="accordion" role="tablist"
                             aria-multiselectable="true">
                            <div class="panel radio panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="booking.html#collapse1"
                                           aria-expanded="true" aria-controls="collapseOne">
                                            <span class="dot"></span> Direct Bank Transfer
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse in" role="tabpanel"
                                     aria-labelledby="heading1">
                                    <div class="panel-body">
                                        <div class="alert alert-success" role="alert">Lorem ipsum dolor sit amet,
                                            consectetur adipiscing elit. Curabitur sollicitudin ultrices suscipit. Sed
                                            commodo vel mauris vel dapibus.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                           href="booking.html#collapse2" aria-expanded="false"
                                           aria-controls="collapseTwo">
                                            <span class="dot"></span> Cheque Payment
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse2" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="heading2">
                                    <div class="panel-body">
                                        Please send your cheque to Store Name, Store Street, Store Town, Store State /
                                        County, Store Postcode.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                           href="booking.html#collapse3" aria-expanded="false"
                                           aria-controls="collapseThree">
                                            <span class="dot"></span> Credit Card
                                        </a>
                                        <span class="overflowed pull-right">
                                    <img src="assets/img/preview/payments/mastercard-2.jpg" alt=""/>
                                    <img src="assets/img/preview/payments/visa-2.jpg" alt=""/>
                                    <img src="assets/img/preview/payments/american-express-2.jpg" alt=""/>
                                    <img src="assets/img/preview/payments/discovery-2.jpg" alt=""/>
                                    <img src="assets/img/preview/payments/eheck-2.jpg" alt=""/>
                                </span>
                                    </h4>
                                </div>
                                <div id="collapse3" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="heading3"></div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading4">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                           href="booking.html#collapse4" aria-expanded="false"
                                           aria-controls="collapse4">
                                            <span class="dot"></span> PayPal
                                        </a>
                                        <span class="overflowed pull-right"><img
                                                src="assets/img/preview/payments/paypal-2.jpg" alt=""/></span>
                                    </h4>
                                </div>
                                <div id="collapse4" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="heading4"></div>
                            </div>
                        </div>

                        <h3 class="block-title alt"><i class="fa fa-angle-down"></i>Additional Information</h3>
                        <form action="booking.html#" class="form-additional">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea name="fad-message" id="fad-message"
                                                  title="Addition information is required" data-toggle="tooltip"
                                                  class="form-control alt" placeholder="Additional Information"
                                                  cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="overflowed reservation-now">
                            <div class="checkbox pull-left">
                                <input id="accept" type="checkbox" name="fd-name" title="Please accept"
                                       data-toggle="tooltip">
                                <label for="accept">I accept all information and Payments etc</label>
                            </div>
                            <a class="btn btn-theme pull-right btn-reservation-now" href="booking.html#">Reservation
                                Now</a>
                            <a class="btn btn-theme pull-right btn-cancel btn-theme-dark"
                               href="booking.html#">Cancel</a>
                        </div>

                    </div>
                    <!-- /CONTENT -->

                    <!-- SIDEBAR -->
                    <aside class="col-md-3 sidebar" id="sidebar">
                        <!-- widget detail reservation -->
                        <div class="widget shadow widget-details-reservation">
                            <h4 class="widget-title">Detail Reservation</h4>
                            <div class="widget-content">
                                <h5 class="widget-title-sub">Picking Up Location</h5>
                                <div class="media">
                                    <span class="media-object pull-left"><i class="fa fa-calendar"></i></span>
                                    <div class="media-body"><p>15 January 2015 / 08:00 am</p></div>
                                </div>
                                <div class="media">
                                    <span class="media-object pull-left"><i class="fa fa-location-arrow"></i></span>
                                    <div class="media-body"><p>From SkyLine AirPort</p></div>
                                </div>
                                <h5 class="widget-title-sub">Droping Off Location</h5>
                                <div class="media">
                                    <span class="media-object pull-left"><i class="fa fa-calendar"></i></span>
                                    <div class="media-body"><p>15 January 2015 / 08:00 am</p></div>
                                </div>
                                <div class="media">
                                    <span class="media-object pull-left"><i class="fa fa-location-arrow"></i></span>
                                    <div class="media-body"><p>From SkyLine AirPort</p></div>
                                </div>
                                <div class="button">
                                    <a href="booking.html#" class="btn btn-block btn-theme btn-theme-dark">Update
                                        Reservation</a>
                                </div>
                            </div>
                        </div>
                        <!-- /widget detail reservation -->
                        <!-- widget testimonials -->
                        <div class="widget shadow">
                            <div class="widget-title">Testimonials</div>
                            <div class="testimonials-carousel">
                                <div class="owl-carousel" id="testimonials">
                                    <div class="testimonial">
                                        <div class="media">
                                            <div class="media-body">
                                                <div class="testimonial-text">Vivamus eget nibh. Etiam cursus leo vel
                                                    metus. Nulla facilisi. Aenean nec eros. Vestibulum ante ipsum primis
                                                    in faucibus orci luctus et ultrices posuere cubilia.
                                                </div>
                                                <div class="testimonial-name">John Doe <span
                                                        class="testimonial-position">Co- founder at Rent It</span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="testimonial">
                                        <div class="media">
                                            <div class="media-body">
                                                <div class="testimonial-text">Vivamus eget nibh. Etiam cursus leo vel
                                                    metus. Nulla facilisi. Aenean nec eros. Vestibulum ante ipsum primis
                                                    in faucibus orci luctus et ultrices posuere cubilia.
                                                </div>
                                                <div class="testimonial-name">John Doe <span
                                                        class="testimonial-position">Co- founder at Rent It</span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="testimonial">
                                        <div class="media">
                                            <div class="media-body">
                                                <div class="testimonial-text">Vivamus eget nibh. Etiam cursus leo vel
                                                    metus. Nulla facilisi. Aenean nec eros. Vestibulum ante ipsum primis
                                                    in faucibus orci luctus et ultrices posuere cubilia.
                                                </div>
                                                <div class="testimonial-name">John Doe <span
                                                        class="testimonial-position">Co- founder at Rent It</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /widget testimonials -->
                        <!-- widget helping center -->
                        <div class="widget shadow widget-helping-center">
                            <h4 class="widget-title">Helping Center</h4>
                            <div class="widget-content">
                                <p>Vivamus eget nibh. Etiam cursus leo vel metus. Nulla facilisi. Aenean nec eros.</p>
                                <h5 class="widget-title-sub">+90 555 444 66 33</h5>
                                <p><a href="mailto:support@supportcenter.com">support@supportcenter.com</a></p>
                                <div class="button">
                                    <a href="booking.html#" class="btn btn-block btn-theme btn-theme-dark">Support
                                        Center</a>
                                </div>
                            </div>
                        </div>
                        <!-- /widget helping center -->
                    </aside>
                    <!-- /SIDEBAR -->

                </div>
            </div>
        </section>
        <!-- /PAGE WITH SIDEBAR -->

        <!-- PAGE -->
        <section class="page-section contact dark">
            <div class="container">

                <!-- Get in touch -->

                <h2 class="section-title">
                    <small>Feel Free to Say Hello!</small>
                    <span>Get in Touch With Us</span>
                </h2>

                <div class="row">
                    <div class="col-md-6">
                        <!-- Contact form -->
                        <form name="contact-form" method="post" action="booking.html#" class="contact-form alt"
                              id="contact-form">

                            <div class="row">
                                <div class="col-md-6">

                                    <div class="outer required">
                                        <div class="form-group af-inner has-icon">
                                            <label class="sr-only" for="name">Name</label>
                                            <input
                                                type="text" name="name" id="name" placeholder="Name" value="" size="30"
                                                data-toggle="tooltip" title="Name is required"
                                                class="form-control placeholder"/>
                                            <span class="form-control-icon"><i class="fa fa-user"></i></span>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">

                                    <div class="outer required">
                                        <div class="form-group af-inner has-icon">
                                            <label class="sr-only" for="email">Email</label>
                                            <input
                                                type="text" name="email" id="email" placeholder="Email" value=""
                                                size="30"
                                                data-toggle="tooltip" title="Email is required"
                                                class="form-control placeholder"/>
                                            <span class="form-control-icon"><i class="fa fa-envelope"></i></span>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group af-inner has-icon">
                                <label class="sr-only" for="input-message">Message</label>
                                <textarea
                                    name="message" id="input-message" placeholder="Message" rows="4" cols="50"
                                    data-toggle="tooltip" title="Message is required"
                                    class="form-control placeholder"></textarea>
                                <span class="form-control-icon"><i class="fa fa-bars"></i></span>
                            </div>

                            <div class="outer required">
                                <div class="form-group af-inner">
                                    <input type="submit" name="submit"
                                           class="form-button form-button-submit btn btn-block btn-theme"
                                           id="submit_btn" value="Send message"/>
                                </div>
                            </div>

                        </form>
                        <!-- /Contact form -->
                    </div>
                    <div class="col-md-6">

                        <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
                            Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.</p>

                        <ul class="media-list contact-list">
                            <li class="media">
                                <div class="media-left"><i class="fa fa-home"></i></div>
                                <div class="media-body">Adress: 1600 Pennsylvania Ave NW, Washington, D.C.</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa"></i></div>
                                <div class="media-body">DC 20500, ABD</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa-phone"></i></div>
                                <div class="media-body">Support Phone: 01865 339665</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa-envelope"></i></div>
                                <div class="media-body">E mails: info@example.com</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa-clock-o"></i></div>
                                <div class="media-body">Working Hours: 09:30-21:00 except on Sundays</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa-map-marker"></i></div>
                                <div class="media-body">View on The Map</div>
                            </li>
                        </ul>

                    </div>
                </div>

                <!-- /Get in touch -->

            </div>
        </section>
        <!-- /PAGE -->

    </div>
    <!-- /CONTENT AREA -->

@endsection
