<?php
/**
 * Created by PhpStorm.
 * User: muturi muraya <muturi.muraya@gmail.com>
 * Date: 02/06/2021
 * Time: 9:39 AM
 * Project car-reservation
 */

?>

<!-- HEADER -->
<header class="header fixed">
    <div class="header-wrapper">
        <div class="container">

            <!-- Logo -->
            <div class="logo">
                <a href="{{ url("") }}"><img src="{{ asset("assets/img/logo-rentit.png") }}" alt="Rent It"/></a>
            </div>
            <!-- /Logo -->

            <!-- Mobile menu toggle button -->
            <a href="{{ url("") }}" class="menu-toggle btn ripple-effect btn-theme-transparent"><i class="fa fa-bars"></i></a>
            <!-- /Mobile menu toggle button -->

            <!-- Navigation -->
            <nav class="navigation closed clearfix">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <!-- navigation menu -->
                        <a href="" class="menu-toggle-close btn"><i class="fa fa-times"></i></a>
                        <ul class="nav sf-menu">
                            <li><a href="{{ url("") }}">Home</a></li>
                            <li><a href="{{ url("about") }}">About Us</a></li>
                            <li><a href="{{ url("cars") }}">Cars</a></li>
                            <li><a href="{{ url("contact") }}">Contact</a></li>
                        </ul>
                        <!-- /navigation menu -->
                    </div>
                </div>
                <!-- Add Scroll Bar -->
                <div class="swiper-scrollbar"></div>
            </nav>
            <!-- /Navigation -->

        </div>
    </div>
</header>
<!-- /HEADER -->

