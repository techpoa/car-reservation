<?php
/**
 * Created by PhpStorm.
 * User: muturi muraya <muturi.muraya@gmail.com>
 * Date: 02/06/2021
 * Time: 9:40 AM
 * Project car-reservation
 */

?>

<!-- PRELOADER -->
<div id="preloader">
    <div id="preloader-status">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
        <div id="preloader-title">Loading</div>
    </div>
</div>
<!-- /PRELOADER -->
