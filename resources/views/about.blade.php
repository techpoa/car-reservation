<?php
/**
 * Created by PhpStorm.
 * User: muturi muraya <muturi.muraya@gmail.com>
 * Date: 02/06/2021
 * Time: 10:30 AM
 * Project car-reservation
 */

?>

@extends("layouts.index")
@section("content")

    <!-- CONTENT AREA -->
    <div class="content-area">

        <!-- BREADCRUMBS -->
        <section class="page-section breadcrumbs text-center">
            <div class="container">
                <div class="page-header">
                    <h1>About Us</h1>
                </div>
                <ul class="breadcrumb">
                    <li><a href="{{ url("") }}">Home</a></li>
                    <li class="active">About</li>
                </ul>
            </div>
        </section>
        <!-- /BREADCRUMBS -->

        <!-- PAGE -->
        <section class="page-section color">
            <div class="container">

                <p class="text-center lead"><strong>Lorem ipsum</strong> dolor sit amet, consectetur adipiscing elit. Morbi fermentum justo vitae convallis varius. Nulla tristique risus ut justo pulvinar mattis. Phasellus aliquet egestas mauris in venenatis. Nulla tristique risus ut justo pulvinar mattis. Phasellus aliquet egestas mauris in venenatis.</p>
                <hr class="page-divider"/>
                <div class="row">
                    <div class="col-md-3">
                        <div class="thumbnail thumbnail-team no-border no-padding">
                            <div class="media">
                                <img src="assets/img/preview/team/team-270x270x1.jpg" alt=""/>
                            </div>
                            <div class="caption">
                                <h4 class="caption-title">Standard Name <small>Support team</small></h4>
                                <ul class="social-icons">
                                    <li><a href="about.html#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="about.html#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="about.html#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="about.html#" class="pinterest"><i class="fa fa-pinterest"></i></a></li>
                                </ul>
                                <div class="caption-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ullamcorper, quam vel viverra laoreet, nibh libero adipiscing diam, sit amet dictum sem nisi ut sapien.</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="thumbnail thumbnail-team no-border no-padding">
                            <div class="media">
                                <img src="assets/img/preview/team/team-270x270x2.jpg" alt=""/>
                            </div>
                            <div class="caption">
                                <h4 class="caption-title">Standard Name <small>Support team</small></h4>
                                <ul class="social-icons">
                                    <li><a href="about.html#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="about.html#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="about.html#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="about.html#" class="pinterest"><i class="fa fa-pinterest"></i></a></li>
                                </ul>
                                <div class="caption-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ullamcorper, quam vel viverra laoreet, nibh libero adipiscing diam, sit amet dictum sem nisi ut sapien.</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="thumbnail thumbnail-team no-border no-padding">
                            <div class="media">
                                <img src="assets/img/preview/team/team-270x270x3.jpg" alt=""/>
                            </div>
                            <div class="caption">
                                <h4 class="caption-title">Standard Name <small>Support team</small></h4>
                                <ul class="social-icons">
                                    <li><a href="about.html#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="about.html#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="about.html#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="about.html#" class="pinterest"><i class="fa fa-pinterest"></i></a></li>
                                </ul>
                                <div class="caption-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ullamcorper, quam vel viverra laoreet, nibh libero adipiscing diam, sit amet dictum sem nisi ut sapien.</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="thumbnail thumbnail-team no-border no-padding">
                            <div class="media">
                                <img src="assets/img/preview/team/team-270x270x4.jpg" alt=""/>
                            </div>
                            <div class="caption">
                                <h4 class="caption-title">Standard Name <small>Support team</small></h4>
                                <ul class="social-icons">
                                    <li><a href="about.html#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="about.html#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="about.html#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="about.html#" class="pinterest"><i class="fa fa-pinterest"></i></a></li>
                                </ul>
                                <div class="caption-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ullamcorper, quam vel viverra laoreet, nibh libero adipiscing diam, sit amet dictum sem nisi ut sapien.</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sagittis blandit elementum. Nullam volutpat vestibulum molestie. Duis ac sapien consequat, sollicitudin diam vitae, fringilla lectus.</p>
                    </div>
                    <div class="col-md-4">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sagittis blandit elementum. Nullam volutpat vestibulum molestie. Duis ac sapien consequat, sollicitudin diam vitae, fringilla lectus.</p>
                    </div>
                    <div class="col-md-4">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sagittis blandit elementum. Nullam volutpat vestibulum molestie. Duis ac sapien consequat, sollicitudin diam vitae, fringilla lectus.</p>
                    </div>
                </div>

            </div>
        </section>
        <!-- /PAGE -->

    </div>
    <!-- /CONTENT AREA -->

@endsection
